const dotenv = require("dotenv");
const express = require("express");

const dayjs = require("dayjs");
const isLeapYear = require("dayjs/plugin/isLeapYear");
dayjs.extend(isLeapYear);
const objectSupport = require("dayjs/plugin/objectSupport");
dayjs.extend(objectSupport);

dotenv.config();

const PORT = process.env.PORT || 56201;

const app = express();

app.use(express.text());
app.use(express.json());

app.post("/square", (req, res) => {
    const number = +req.body;

    if (isNaN(number)) {
        return res.status(400).json({ message: "Validation error" });
    }

    res.json({ number, square: number * number });
});

app.post("/reverse", (req, res) => {
    const text = req.body;

    res.setHeader("content-type", "text/plain");
    return res.send(text.split("").reverse().join(""));
});


app.get("/date/:year/:month/:day", (req, res) => {
    const { year, month, day } = req.params;

    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    const date = dayjs(`${year}-${month}-${day}`);
    const now = dayjs({ h: 0, m: 0, s: 0, ms: 0 });

    const weekDay = days[date.day()];
    const isLeapYear = date.isLeapYear();
    const difference = Math.abs(date.diff(now, "day"));

    return res.json({
        weekDay,
        isLeapYear,
        difference,
    });
});

app.listen(PORT, () => {
    console.log(`App started on port: ${PORT}`);
});


